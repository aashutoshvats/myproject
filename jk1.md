Consider the scenario where you have a text file in your repository 'repo1' containing the details about the ninja batch x in the tabular format like
S. No.    Name    Batch    Tool
1        Naveen       x        Jenkins
2        Mohit      x        Sonarqube
We have one more repository 'repo2' which contains a shell script that would read a file and then print only the 'Name' and the 'Tool' column
We have to setup a Jenkins job which would clone the repo1 and as the part of my build I would like the repo 2 as well to be cloned and the shell script should be executed
![](image/jk111)
![](image/jk112)
![](image/jk113)
![](image/jk114)
Consider a scenario where you are having around 10 jobs in your jenkins instance and all the jobs are having around 30 builds in their build history, now we would like to clean the clutter but not by manually going and removing every build but by using a more automated approach, discover if there is a way we can do this in jenkins and implement the same.
![](image/jk12)
Consider a scenario where you have a build history of failed and successful build, discover a way in jenkins to visualize this in a better way instead of going through the status globes everytime.
![](image/jk131)
![](image/jk132)


