Consider a scenario where you have created a script which will install a certain package on your machine and we would like to automate that process using Jenkins but considering an organization perspective we would have 3 functional users in Jenkins as
- DevOps

- Dev
- Qa
The user DevOps is able to manage the Jenkins instance globally
The user Dev is able to manage the jobs in Jenkins
The user QA is only able to execute the jobs nothing else
You should configure a custom e-mail notification that would send the details about the execution like:
- Job name
- Build number
- who triggered the build
- Execution status
![](image/jk211.png)

![](image/jk212.png)

![](image/jk411.png)
![](image/jk412.png)
![](image/jk413.png)
![](image/jk414.png)
![](image/jk415.png)
![](image/jk416.png)
![](image/jk417.png)
![](image/jk418.png)
![](image/jk419.png)

![](image/jk213.png)
![](image/jk214.png)
![](image/jk215.png)
![](image/jk216.png)
![](image/jk217.png)









